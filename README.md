docker-ubuntu-vnc-desktop-ml
============================
Images to provide HTML5 and VNC interface to access Ubuntu 18.04 (bionic) LXDE desktop environment with keras, tesnorflow, jupyter and other tools inside. 

Without CUDA, only CPU support!!!

Basen on
-------------------------
https://github.com/fcwu/docker-ubuntu-vnc-desktop 

Images
------
-  yorick/ml-base:cpu - base image with ML libs
-  yorick/ml-gym:cpu - image with ML libs and OpenAI GYM (without mujoco)

General aim
-----------
Allow to run OpenAI gym rendering inside container and launch experiments from jupyter notebook in local browser. Also allow visual control of execution by VNC or just by browser.

Common execute options
----------------------
```
docker run -p 6080:80 -p 5900:5900 -p 8888:8888 -v /home/user/notebook/:/notebook yorick/ml-gym:cpu
```

where options are
- -p 6080:80 - connect to container by browser
- -p 5900:5900 - connect to container by VNC client
- -p 8888:8888 - jupyter notebook
- -v /home/user/notebook/:/notebook - mount local notebook folder

Quick Start
-----------

Run the docker container and access with port `6080`

```
docker run -p 6080:80 yorick/ml-base:cpu
```

Browse http://127.0.0.1:6080/

VNC Viewer
------------------

Forward VNC service port 5900 to host by

```
docker run -p 6080:80 -p 5900:5900 yorick/ml-base:cpu
```

Now, open the vnc viewer and connect to port 5900. If you would like to protect vnc service by password, set environment variable `VNC_PASSWORD`, for example

```
docker run -p 6080:80 -p 5900:5900 -e VNC_PASSWORD=mypassword yorick/ml-base:cpu
```

A prompt will ask password either in the browser or vnc viewer.

HTTP Base Authentication
---------------------------

This image provides base access authentication of HTTP via `HTTP_PASSWORD`

```
docker run -p 6080:80 -e HTTP_PASSWORD=mypassword yorick/ml-base:cpu
```

SSL
--------------------

To connect with SSL, generate self signed SSL certificate first if you don't have it

```
mkdir -p ssl
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ssl/nginx.key -out ssl/nginx.crt
```

Specify SSL port by `SSL_PORT`, certificate path to `/etc/nginx/ssl`, and forward it to 6081

```
docker run -p 6081:443 -e SSL_PORT=443 -v ${PWD}/ssl:/etc/nginx/ssl yorick/ml-base:cpu
```

Screen Resolution
------------------

The Resolution of virtual desktop adapts browser window size when first connecting the server. You may choose a fixed resolution by passing `RESOLUTION` environment variable, for example

```
docker run -p 6080:80 -e RESOLUTION=1920x1080 yorick/ml-base:cpu
```

Default Desktop User
--------------------

The default user is `root`. You may change the user and password respectively by `USER` and `PASSWORD` environment variable, for example,

```
docker run -p 6080:80 -e USER=doro -e PASSWORD=password yorick/ml-base:cpu
```

Other important information
---------------------------
Look at https://github.com/fcwu/docker-ubuntu-vnc-desktop 

License
==================

See the LICENSE file for details.